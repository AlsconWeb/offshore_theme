<?php get_header(); ?>
    <div id="container">
        <div id="content">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="post">
                    <h1><?php the_title(); ?></h1>
                    <div class="entry">
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php endwhile; endif; ?>
                    <?php edit_post_link('Редактировать.', '<p>', '</p>'); ?>

                        <?php get_sidebar(); ?>
                            <?php get_footer(); ?>