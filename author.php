<?php get_header(); ?>
    <div id="container">
        <div id="content">
            <?php
	if(isset($_GET['author_name'])) :
		$curauth = get_userdatabylogin($author_name);
	else :
		$curauth = get_userdata(intval($author));
	endif;
?>

                <h1 class="pagetitle">Страница автора "<?php echo $curauth->display_name; ?>"</h1>

                <p><strong>Ник автора:</strong>
                    <?php echo $curauth->nickname; ?>
                        <br />

                        <?php if(!empty($curauth->first_name)) { ?><strong>Имя:</strong>
                            <?php echo $curauth->first_name; ?>
                                <?php echo $curauth->last_name; ?>
                                    <br />
                                    <?php } ?>

                                        <?php if($curauth->user_url != 'http://') { ?><strong>Сайт:</strong> <a href="<?php echo $curauth->user_url; ?>" target="_blank"><?php echo $curauth->user_url; ?></a>
                                            <br />
                                            <?php } ?>

                                                <?php if(!empty($curauth->description)) { ?><strong>Об авторе:</strong>
                                                    <?php echo $curauth->description; ?>
                                                        <?php } ?>
                </p>

                <br />

                <h3>Список статей автора:</h3>

                <?php
  $order = "&orderby=cost&order=DESC";
  $s2 = ' selected="selected"';
  if ($_POST['select'] == 'title') { $order = "&orderby=title&order=ASC"; $s1 = ' selected="selected"'; $s2 = ''; }
  if ($_POST['select'] == 'newest') { $order = "&orderby=cost&order=DESC"; $s2 = ' selected="selected"'; }
  if ($_POST['select'] == 'oldest') { $order = "&orderby=cost&order=ASC"; $s3 = ' selected="selected"'; $s2 = ''; }
?>

                    <form method="post" id="order">
                        Сортировка:
                        <select name="select" onchange='this.form.submit()'>
                            <option value="title" <?=$s1?>>По заголовку</option>
                            <option value="newest" <?=$s2?>>Самые новые</option>
                            <option value="oldest" <?=$s3?>>Самые старые</option>
                        </select>
                    </form>

                    <div class="line"></div>

                    <?php if (is_author()) { ?>
                        <?php $posts = query_posts($query_string . $order); ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="post">
                                    <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                    <div class="postmetadata">
                                        <?php the_time('d.m.Y') ?> |
                                            <?php comments_popup_link('Оставить комментарий', 'Комментариев: 1', 'Комментариев: %'); ?>
                                    </div>
                                    <div class="entry">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                                <?php endwhile; ?>
                                    <?php } ?>

                                        <div class="navigation">
                                            <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
                                                <div class="alignleft">
                                                    <?php next_posts_link('&laquo; Раньше') ?>
                                                </div>
                                                <div class="alignright">
                                                    <?php previous_posts_link('Позже &raquo;') ?>
                                                </div>
                                                <?php } ?>
                                        </div>

                                        <?php get_sidebar(); ?>
                                            <?php get_footer(); ?>