<?php
@ini_set( 'mysqli.allow_local_infile', 'on' );

function offshoreScripts() {
	wp_enqueue_script( 'offshore-functions', get_template_directory_uri() . '/js/functions.js', false, '', true );
	wp_localize_script( 'offshore-functions', 'offshore_ajax', [
		'ajax_url' => admin_url( 'admin-ajax.php' ),
	] );
}

add_action( 'wp_enqueue_scripts', 'offshoreScripts' );

add_filter( 'comments_template', 'legacy_comments' );
function legacy_comments( $file ) {
	if ( ! function_exists( 'wp_list_comments' ) ) : // WP 2.7-only check
		$file = TEMPLATEPATH . '/comments-old.php';
	endif;

	return $file;
}

remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
add_theme_support( 'automatic-feed-links' );
remove_action( 'wp_head', 'feed_links' );
remove_action( 'wp_head', 'feed_links_extra' );
remove_action( 'template_redirect', 'wp_shortlink_header', 11 );
header_remove( 'x-powered-by' );

function ny_remove_x_pingback( $headers ) {
	unset( $headers['X-Pingback'] );

	return $headers;
}

add_filter( 'wp_headers', 'ny_remove_x_pingback' );

if ( function_exists( 'register_sidebar' ) ) {
	register_sidebar( [
		'name'          => 'Сайдбар 1',
		'id'            => "sidebar-1",
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<p><b>',
		'after_title'   => '</b></p>',
	] );
}

if ( function_exists( 'register_sidebar' ) ) {
	register_sidebar( [
		'name'          => 'Сайдбар 2',
		'id'            => "sidebar-2",
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<p><b>',
		'after_title'   => '</b></p>',
	] );
}


function get_wp_vers() {
	$wp_version = file_get_contents( ABSPATH . "wp-includes/version.php" );
	preg_match( "/'(.*)'/is", $wp_version, $out );
	$out = $out[1];
	preg_match( "/\d\.\d/i", $out, $match );

	return $match[0];
}


/* Количество постов */
if ( get_wp_vers() < '2.3' ) {
	$ptype = '';
} else {
	$ptype = " AND post_type = 'post'";
}

$numposts = $wpdb->get_var( "SELECT COUNT(*) FROM " . $wpdb->posts . " WHERE post_status = 'publish'" . $ptype );
if ( 0 < $numposts ) {
	$numposts = number_format( $numposts );
}
$numposts = preg_replace( "','", '', $numposts );

/* Количество авторов */
$users = $wpdb->get_var( "SELECT COUNT(*) FROM " . $wpdb->users );
if ( 0 < $users ) {
	$users = number_format( $users );
}
$users = preg_replace( "','", '', $users );


function declension( $int, $expressions ) {
	settype( $int, "integer" );
	$count = $int % 100;
	if ( $count >= 5 && $count <= 20 ) {
		$result = "<strong>" . $int . "</strong> " . $expressions['2'];
	} else {
		$count = $count % 10;
		if ( $count == 1 ) {
			$result = " <strong>" . $int . "</strong> " . $expressions['0'];
		} elseif ( $count >= 2 && $count <= 4 ) {
			$result = " <strong>" . $int . "</strong> " . $expressions['1'];
		} else {
			$result = " <strong>" . $int . "</strong> " . $expressions['2'];
		}
	}

	return $result;
}

function true_apply_tags_for_pages() {
	add_meta_box( 'tagsdiv-post_tag', 'Теги', 'post_tags_meta_box', 'page', 'side', 'normal' ); // сначала добавляем метабокс меток
	register_taxonomy_for_object_type( 'post_tag', 'page' ); // затем включаем их поддержку страницами wp
}

add_action( 'admin_init', 'true_apply_tags_for_pages' );

function true_expanded_request_post_tags( $q ) {
	if ( isset( $q['tag'] ) ) // если в запросе присутствует параметр метки
	{
		$q['post_type'] = [ 'post', 'page' ];
	}

	return $q;
}

function register_my_session() {
	if ( ! session_id() ) {
		session_start();
	}
}

add_action( 'init', 'register_my_session' );

add_filter( 'request', 'true_expanded_request_post_tags' );

function declens( $int, $expressions ) {
	settype( $int, "integer" );
	$count = $int % 100;
	if ( $count >= 5 && $count <= 20 ) {
		$result = "зарегистрировано <strong>" . $int . "</strong> " . $expressions['2'];
	} else {
		$count = $count % 10;
		if ( $count == 1 ) {
			$result = "зарегистрирован <strong>" . $int . "</strong> " . $expressions['0'];
		} elseif ( $count >= 2 && $count <= 4 ) {
			$result = "зарегистрировано <strong>" . $int . "</strong> " . $expressions['1'];
		} else {
			$result = "зарегистрировано <strong>" . $int . "</strong> " . $expressions['2'];
		}
	}

	return $result;
}

add_action( 'save_post', 'my_template_thumbnail' );
function my_template_thumbnail( $post_id ) {
	$post_thumbnail = get_post_meta( $post_id, $key = '_thumbnail_id', $single = true );
	if ( ! wp_is_post_revision( $post_id ) ) : if ( empty( $post_thumbnail ) ) {
		update_post_meta( $post_id, $meta_key = '_thumbnail_id', $meta_value = '42471' );
	} endif;
}


add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) {
	global $wpdb;
	$user_data = $wpdb->get_results( "SELECT * FROM `wp_subscribe_data` WHERE `user_id` = " . (int) $_GET['user_id'] . " ORDER BY `id` DESC" );

	echo "<h3>Подписан на компании</h3>";
	echo '<table class="form-table">
            <tr>
                <th>ID</label></th>
                <th>Name</th>
            </tr>';
	if ( $user_data ) {
		foreach ( $user_data as $item ) {
			echo "<tr>";
			echo "<td>{$item->company_id}</td>";
			echo "<td>{$item->company_name}</td>";
			echo "</tr>";
		}
	}
	echo "</table>";
}

if ( ! wp_next_scheduled( 'my_sync_gb_companies' ) ) {
	wp_schedule_event( time(), 'daily', 'my_sync_gb_companies' );
}
if ( function_exists( 'fillCompaniesHistoryTable' ) ) {
	add_action( 'my_sync_gb_companies', 'fillCompaniesHistoryTable' );
}

function is_user_role( $role, $user_id = null ) {
	$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
	if ( ! $user ) {
		return false;
	}

	return in_array( $role, (array) $user->roles );
}

function hideAdminBar() {
	if ( is_user_role( 'subscriber' ) ) {
		add_filter( 'show_admin_bar', '__return_false' );
	}
}

add_action( 'init', 'hideAdminBar' );

function updateSubscriber() {
	$userdata = $_POST['userdata'];
	$user     = get_userdata( $userdata['ID'] );
	if ( ( ! empty( $_POST['old_pass'] ) && ! empty( $userdata['user_pass'] ) ) && ! wp_check_password( $_POST['old_pass'], $user->user_pass, $userdata['ID'] ) ) {
		wp_send_json( [
			'status'  => 0,
			'message' => "Старый пароль указан не верно",
		] );
	} else {
		if ( wp_update_user( $userdata ) ) {
			do_action( 'edit_user_profile_update', $userdata['ID'] );
			do_action( 'personal_options_update' );
			do_action( 'edit_user_profile_update' );
			wp_send_json( [
				'status'  => 1,
				'message' => "Данные успешно обновлены",
			] );
		} else {
			wp_send_json( [
				'status'  => 0,
				'message' => "Возникла ошибка",
			] );
		}
	}
}

add_action( 'wp_ajax_nopriv_updateSubscriber', 'updateSubscriber' );
add_action( 'wp_ajax_updateSubscriber', 'updateSubscriber' );

function getUserAvatar() {
	$res       = '<img src="' . get_template_directory_uri() . "/i/photo_camera.svg" . '" width="40" height="32" alt="photo-camera">';
	$user_meta = get_user_meta( get_current_user_id() );
	if ( isset( $user_meta['simple_local_avatar'] ) ) {
		$res = '<img src="' . unserialize( $user_meta['simple_local_avatar'][0] )['full'] . '">';
	}

	return $res;
}

function getUserSubscribeData() {
	global $wpdb;

	return $wpdb->get_results( "SELECT * FROM `wp_subscribe_data` WHERE `user_id` = " . (int) get_current_user_id() . " ORDER BY `id` DESC" );
}

function unSubscribe() {
	global $wpdb;
	$fields['company_id'] = $_POST['id'];
	$fields['user_id']    = get_current_user_id();
	$wpdb->delete( 'wp_subscribe_data', $fields );
}

add_action( 'wp_ajax_nopriv_unSubscribe', 'unSubscribe' );
add_action( 'wp_ajax_unSubscribe', 'unSubscribe' );


function remove_canonical_url( $url ) {

	if ( is_page( 38349 ) ) {
		return false;
	}

	return $url;
}

add_filter( 'aioseo_canonical_url', 'remove_canonical_url', 20, 1 );


