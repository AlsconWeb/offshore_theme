<?php
    /**
     * Template Name: Profile
     *
     * Created by PhpStorm.
     * User: macbook
     * Date: 16.09.18
     * Time: 18:24
     */

    $user = get_userdata(get_current_user_id());
    if ($user):
?>

<?php get_header(); ?>
<div id="container">
    <div id="content">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="post">
                <h1><?php the_title(); ?></h1>
                <div class="entry">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endwhile; endif; ?>

        <div id="profile">
            <h2>Редактирование профиля</h2>
            <form action="<?= get_permalink(44275) ?>" class="js-profile-form" method="POST" enctype="multipart/form-data">
                <label for="name"> Имя:
                    <input type="text" name="userdata[first_name]" name="userdata[first_name]" placeholder="Username" value="<?= $user->first_name ?>" id="name"/>
                </label>
                <label for="second-name"> Фамилия:
                    <input type="text" name="userdata[last_name]" placeholder="Second name" value="<?= $user->last_name ?>" id="second-name"/>
                </label>
                <label for="nickname"> Ник <span>(обязательно):</span>
                    <input type="text" name="userdata[display_name]" placeholder="Nickname" id="nickname" value="<?= $user->display_name ?>" required/>
                </label>
                <label for="email"> E-mail <span>(обязательно):</span>
                    <input type="text" name="userdata[user_email]" placeholder="Username@example.com" value="<?= $user->user_email ?>" id="email" required/>
                </label>
                <div class="left-block">
                    <div class="img">
                        <?= getUserAvatar() ?>
                    </div>
                    <div class="input-file">
                        <span>Выберите файл на своём компьютере:</span>
                        <input id="add-photo" name="simple-local-avatar" type="file">
                        <label for="add-photo">Загрузить фото</label>
                    </div>
                </div>
                <label> Изменение пароля:
                    <input type="password" name="old_pass" placeholder="Старый пароль"/>
                    <input type="password" class="js-pass" name="userdata[user_pass]" placeholder="Новый пароль"/>
                    <input type="password" class="js-pass-confirm" placeholder="Подтвердить новый пароль"/>
                </label>
                <input type="hidden" name="userdata[ID]" value="<?= $user->ID ?>"/>
                <?= wp_nonce_field('simple_local_avatar_nonce', '_simple_local_avatar_nonce', false); ?>
                <input type="hidden" name="action" value="updateSubscriber"/>
                <div class="js-message" style="display: none">Пароли не совпадают</div>
                <input type="submit" value="Обновить профиль"/>
            </form>
        </div><!-- end #profile-->

        <?php else:
            global $wp_query;
            $wp_query->set_404();
            status_header(404);
            include(get_query_template('404'));
            die();
            endif;
        ?>
