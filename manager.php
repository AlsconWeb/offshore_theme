<?php
    /**
     * Template Name: Manager
     *
     * Created by PhpStorm.
     * User: macbook
     * Date: 16.09.18
     * Time: 18:24
     */

    $user = get_userdata(get_current_user_id());
    if ($user):
    get_header(); ?>
<div id="container">
    <div id="content">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="post">
            <h1><?php the_title(); ?></h1>
            <div class="entry">
                <?php the_content(); ?>
            </div>
        </div>
        <?php endwhile; endif; ?>

        <div id="manager">
            <h3>Подписка на компании:</h3>
            <table>
                <tr>
                    <th>
                        <span>Date</span></th>
                    <th>
                        <span>ID</span></th>
                    <th>
                        <span>Company name</span></th>
                </tr>

                <?php
                    $data = getUserSubscribeData();
                    if ($data):
                        foreach ($data as $item): ?>
                <tr>
                    <td><?= date('d.m.Y', strtotime($item->created_at)) ?></td>
                    <td><?= $item->company_id ?></td>
                    <td>
                        <a href="<?= get_permalink(38349) ?>/<?= $item->company_id ?>"><?= $item->company_name ?></a>
                        <button class="remove js-remove" data-id="<?= $item->company_id ?>"><img src="<?= get_template_directory_uri() ?>/i/error.svg"
                                alt="remove button"></button>
                    </td>
                </tr>
                <?php endforeach;
                    endif; ?>
            </table>
        </div><!-- end #manager-->

        <?php
            get_sidebar();
            get_footer();
            else:
                global $wp_query;
                $wp_query->set_404();
                status_header(404);
                include(get_query_template('404'));
                die();
            endif;
        ?>