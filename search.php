<?php get_header(); ?>
    <div id="container">
        <div id="content">
            <?php if (have_posts()) : ?>

                <h1 class="pagetitle">Результаты поиска по запросу "<?php the_search_query(); ?>"</h1>

                <?php while (have_posts()) : the_post(); ?>

                    <div class="post">
                        <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                        <div class="postmetadata">
                            <?php the_time('d.m.Y') ?> | Автор:
                                <a href="<?php bloginfo('url'); ?>/author/<?php the_author_login(); ?>/">
                                    <?php the_author() ?>
                                </a> | Рубрика:
                                <?php the_category(', ') ?> |
                                    <?php comments_popup_link('Оставить комментарий', 'Комментариев: 1', 'Комментариев: %'); ?>
                        </div>
                        <div class="entry">
                            <?php the_excerpt(); ?>
                        </div>
                    </div>

                    <?php endwhile; ?>

                        <div class="navigation">
                            <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
                                <div class="alignleft">
                                    <?php next_posts_link('&laquo; Раньше') ?>
                                </div>
                                <div class="alignright">
                                    <?php previous_posts_link('Позже &raquo;') ?>
                                </div>
                                <?php } ?>
                        </div>

                        <?php else : ?>

                            <h2 class="center">Ничего не найдено. Попробуйте другой критерий поиска.</h2>
                            <?php include (TEMPLATEPATH . '/searchform.php'); ?>

                                <?php endif; ?>

                                    <?php get_sidebar(); ?>
                                        <?php get_footer(); ?>