<?php get_header(); ?>
    <div id="container">
        <div id="content">
            <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
                <?php if (is_category()) { ?>
                    <h1 class="pagetitle">Содержимое рубрики "<?php echo single_cat_title(); ?>"</h1>
                    <?php } ?>


                        <div class="nav">
                            <a href="<?php bloginfo('url'); ?>/">Главная</a> &raquo;
                            <?php echo(get_category_parents($cat, TRUE, ' &raquo; ')); ?>
                        </div>


                        <?php if (get_categories('child_of='.$cat.'&hide_empty=0')) { ?>
                            <h3>Подрубрики:</h3>
                            <ul class="child-categories">
                                <?php wp_list_categories('show_count=1&child_of='.$cat.'&hide_empty=0&title_li='); ?>
                            </ul>
                            <?php	} else { ?>
                                <?php
	  $ID = $wp_query->posts[0]->ID;
	  $postcat = get_the_category($ID);
	  $cat = $postcat[0]->cat_ID;
	  $parent = get_category($cat);
	?>

                                    <?php if ($parent->parent) { ?>
                                        <h3>Соседние рубрики:</h3>
                                        <ul class="child-categories">
                                            <?php error_reporting(0); wp_list_categories ('show_count=1&child_of='.$parent->parent.'&exclude='.$cat.'&hide_empty=0&title_li='); ?>
                                        </ul>
                                        <?php } ?>
                                            <?php	} ?>


                                                <?php
  $order = "&orderby=cost&order=DESC";
  $s2 = ' selected="selected"';
  if ($_POST['select'] == 'title') { $order = "&orderby=title&order=ASC"; $s1 = ' selected="selected"'; $s2 = ''; }
  if ($_POST['select'] == 'newest') { $order = "&orderby=cost&order=DESC"; $s2 = ' selected="selected"'; }
  if ($_POST['select'] == 'oldest') { $order = "&orderby=cost&order=ASC"; $s3 = ' selected="selected"'; $s2 = ''; }
?>

                                                    <form method="post" id="order">
                                                        Сортировка:
                                                        <select name="select" onchange='this.form.submit()'>
                                                            <option value="title" <?=$s1?>>По заголовку</option>
                                                            <option value="newest" <?=$s2?>>Самые новые</option>
                                                            <option value="oldest" <?=$s3?>>Самые старые</option>
                                                        </select>
                                                    </form>


                                                    <div class="line"></div>

                                                    <?php if (have_posts()) : ?>
                                                        <?php $posts = query_posts($query_string . $order); ?>
                                                            <?php while (have_posts()) : the_post(); ?>

                                                                <div class="post">
                                                                    <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                                                    <div class="postmetadata">
                                                                        <?php the_time('d.m.Y') ?> | Автор:
                                                                            <a href="<?php bloginfo('url'); ?>/author/<?php the_author_login(); ?>/">
                                                                                <?php the_author() ?>
                                                                            </a> | Рубрика:
                                                                            <?php the_category(', ') ?> |
                                                                                <?php comments_popup_link('Оставить комментарий', 'Комментариев: 1', 'Комментариев: %'); ?>
                                                                    </div>
                                                                    <div class="entry">
                                                                        <?php the_excerpt(); ?>
                                                                    </div>
                                                                </div>

                                                                <?php endwhile; ?>

                                                                    <div class="navigation">
                                                                        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
                                                                            <div class="alignleft">
                                                                                <?php next_posts_link('&laquo; Раньше') ?>
                                                                            </div>
                                                                            <div class="alignright">
                                                                                <?php previous_posts_link('Позже &raquo;') ?>
                                                                            </div>
                                                                            <?php } ?>
                                                                    </div>

                                                                    <?php else : ?>
                                                                        <h3>В данной рубрике нет статей.</h3>
                                                                        <?php endif; ?>

                                                                            <?php get_sidebar(); ?>
                                                                                <?php get_footer(); ?>