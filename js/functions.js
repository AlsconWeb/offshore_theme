/**
 * Created by macbook on 16.09.18.
 */
jQuery(document).ready(function ($) {
    $('.js-profile-form').on('submit', function () {
        var _this = $(this),
            data = new FormData($(_this)[0]);
        $(_this).find('.js-message').hide();
        if ($('.js-pass').val() != '' && $('.js-pass').val() != $('.js-pass-confirm').val()) {
            $(_this).find('.js-message').text('Пароли не совпадают').show();
            return false;
        }
        $.ajax({
            url        : '/blog/wp-admin/admin-ajax.php',
            type       : 'POST',
            data       : data,
            contentType: false,
            processData: false,
            success    : function (data) {
                if (typeof data != 'undefined' && typeof data.status != 'undefined') {
                    switch (data.status) {
                        case 1:
                            $(_this)[0].reset();
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000)
                        case 0:
                            $(_this).find('.js-message').text(data.message).show();
                            break;
                    }
                }
            }
        })
        return false;
    })

    $('.js-remove').on('click', function (e) {
        var _this = $(this);
        e.preventDefault();
        $.ajax({
            url    : '/blog/wp-admin/admin-ajax.php',
            type   : 'POST',
            data   : {
                id      : $(_this).data('id'),
                'action': 'unSubscribe'
            },
            success: function () {
                window.location.reload();
            }
        })
        return false;
    })
})